class prezto {
  
 #include vcsrepo
  
  # load prezto
  vcsrepo { "/home/vagrant/.zprezto":
      ensure   => latest,
      owner    => $owner,
      group    => $owner,
      provider => git,
      source   => "https://github.com/sorin-ionescu/prezto.git",
      revision => 'master',
  }

  # copy prezto files
  file { '/home/vagrant/.zlogin':
    ensure => 'present',
    source => '/home/vagrant/.zprezto/runcoms/zlogin',
  }

  file { '/home/vagrant/.zlogout':
    ensure => 'present',
    source => '/home/vagrant/.zprezto/runcoms/zlogout',
  }

  file { '/home/vagrant/.zpreztorc':
    ensure => 'present',
    source => '/home/vagrant/.zprezto/runcoms/zpreztorc',
  }

  file { '/home/vagrant/.zprofile':
    ensure => 'present',
    source => '/home/vagrant/.zprezto/runcoms/zprofile',
  }

  file { '/home/vagrant/.zshenv':
    ensure => 'present',
    source => '/home/vagrant/.zprezto/runcoms/zshenv',
  }

  file { '/home/vagrant/.zshrc':
    ensure => 'present',
    source => '/home/vagrant/.zprezto/runcoms/zshrc',
  }
  
}